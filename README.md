# Project 4: Brevet time calculator with Ajax

A Reimplementation of the RUSA ACP controle time calculator with flask and ajax.

Author: Joe Webb

Credits to Michal Young for the initial version of this code.

## README

This implementation of the ACP controle time algorithm is slightly different than the one found in the link below. It possesses the same functionality, but works in a slightly different manor. the open and close times are filled as soon as the user hits enter or clicks another box in the table. Since the brevet times are calculated independently, order doesn't matter. The user will be alerted of invalid input by 'notes'. The timezone can be changed by the ENV variable in the docker file, but should not effect the results of the algorithm. This implementation follows the same rules as described in the links below. Tests from official examples can be found in the brevet/tests. To run the tests, enter 'nosetests' while in the brevets directory.

## To Build:

Navigate to the brevets directory, run the script 'run.sh' by entering './run.sh' in bash to build and start the docker container, or just run flask_brevets.py with python3 if docker isn't your thing.


## ACP controle times   

The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator). Additional background information is given here (https://rusa.org/pages/rulesForRiders). The description is ambiguous, but the examples help. Part of finishing this project is clarifying anything that is not clear about the requirements, and documenting it clearly.  

We are essentially replacing the calculator here (https://rusa.org/octime_acp.html). We can also use that calculator to clarify requirements and develop test data.  
