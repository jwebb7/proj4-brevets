"""
Nose tests for acp_times.py

"""
from acp_times import open_time, close_time
import arrow
import nose

time = arrow.get('2019-01-01 00:00', 'YYYY-MM-DD HH:mm',tzinfo='local').isoformat()

# all testing information is from examples in the documentation
# controls bigger than 120% of the brevet distance were not tested because
# the javascript takes care of it.

def test_open_acp_times():
    open1 = open_time(890,1000,time)
    assert open1 == arrow.get(time).shift(hours=+29, minutes=+9).isoformat()
    open2 = open_time(60,200,time)
    assert open2 == arrow.get(time).shift(hours=+1, minutes=+46).isoformat()

def test_close_acp_times():
    close1 = close_time(890,1000,time)
    assert close1 == arrow.get(time).shift(hours=+65, minutes=+23).isoformat()
    close2 = close_time(60,200,time)
    assert close2 == arrow.get(time).shift(hours=+4).isoformat()

def test_overlap_acp_times():
    open = open_time(205,200,time)
    assert open == arrow.get(time).shift(hours=+5, minutes=+53).isoformat()
    close = close_time(205,200,time)
    assert close == arrow.get(time).shift(hours=+13, minutes=+30).isoformat()


def test_close_zero_acp_times():
    close1 = close_time(0,200,time)
    assert close1 == arrow.get(time).shift(hours=+1).isoformat()
    close2 = close_time(0,1000,time)
    assert close2 == arrow.get(time).shift(hours=+1).isoformat()

def test_close_max_acp_times():
    max1 = close_time(300,300,time)
    assert max1 == arrow.get(time).shift(hours=+20).isoformat()
    max2 = close_time(400,400,time)
    assert max2 == arrow.get(time).shift(hours=+27).isoformat()
    max3 = close_time(1000,1000,time)
    assert max3 == arrow.get(time).shift(hours=+75).isoformat()
